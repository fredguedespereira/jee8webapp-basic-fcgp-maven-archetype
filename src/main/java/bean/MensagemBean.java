package ${package}.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;


import br.edu.ifpb.pweb2.bean.GenericBean;
import ${package}.model.Mensagem;

@Named(value = "mensagemBean")
@ViewScoped
public class MensagemBean extends GenericBean implements Serializable {
private static final long serialVersionUID = 1L;
	
	private Mensagem mensagem;
	
	// Não implemente construtores em backing beans, use esta anotação sobre algum método
	@PostConstruct
	public void init() {
		this.mensagem = new Mensagem("Olá, mundo JSF 2.3!");
	}
	
	public Mensagem getMensagem() {
		return mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}
}

